var fs = require('fs');
var csv = require('fast-csv');
var Houndify = require('houndify');
var configFile = './config.json';
var config = require(path.join(__dirname, configFile));

var inputArr = []
fs.createReadStream('TestInput.csv')
  .pipe(csv())
  .on('data', function(data){
    let rowData = {};

      Object.keys(data).forEach(current_key => {
         rowData[current_key] = data[current_key]
      });

      inputArr.push(rowData);

  })
  .on('end', function(data){
    for(index=2; index <= inputArr.length-2; index++) {
      console.log(inputArr[index][1])
      var textRequest = new Houndify.TextRequest({

        query: inputArr[index]['0'],
      
        clientId:  config.clientId, 
        clientKey: config.clientKey,
      
        requestInfo: {
          UserID: "test_user",
          Latitude: 37.388309, 
          Longitude: -121.973968
        },
      
        onResponse: function(response, info) {
          console.log(response['AllResults'][0]['CommandKind']);
          if (response['AllResults'][0]['CommandKind'] === inputArr[index][1])
          {
      
        
              // console.log(response['AllResults'][0]['CommandKind'])
              console.log("PASSED")
          }
        },
      
        onError: function(err, info) {
          console.log(err);
        }
      
        });
    }
  

  });

